## Tiktok without watermark 
## 抖音无水印视频获取方法-一分钟完成

- 本项目不是教你如何编程,而是指引你完成一项具体事务


[视频过程点击这里](https://gitee.com/giteejun/TiktokWithoutWatermark/blob/master/%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B.mov)

![alt 在浏览器中打开链接应该是这样的](./img/视频过程.gif "在浏览器中打开链接应该是这样的")

## 1. 得到一个抖音分享链接(从抖音app复制链接)

比如:  #萝莉 https://v.douyin.com/3kXEgJ/ 复制此链接，打开【抖音短视频】，直接观看视频！

在浏览器中打开链接应该是这样的
![alt 在浏览器中打开链接应该是这样的](./img/1.jpg "在浏览器中打开链接应该是这样的")

## 2. 在postman粘贴链接
比如: https://v.douyin.com/3kXEgJ/

![alt 在postman中打开链接应该是这样的](./img/2.jpg "在postman中打开链接应该是这样的")

## 3. 在得到的html最底部找到playAddr的值

比如: https://aweme.snssdk.com/aweme/v1/playwm/?s_vid=93f1b41336a8b7a442dbf1c29c6bbc561645be9ccc818dd0da9c4347e772687276959ac6d5f36892bb19a42cbe63879d264fa3160275b93b82f7990a152416e1&line=0

![alt 在得到的html最底部找到playAddr的值](./img/3.jpg "在得到的html最底部找到playAddr的值")

## 4. 打开这个链接得到带水印的视频

你可以看到这是一个带水印的视频

![alt 打开这个链接得到带水印的视频](./img/4.jpg "打开这个链接得到带水印的视频")



## 5. 将链接修改为不带水印的链接,只需要将playwm修改为play

比如: https://aweme.snssdk.com/aweme/v1/play/?s_vid=93f1b41336a8b7a442dbf1c29c6bbc561645be9ccc818dd0da9c4347e772687276959ac6d5f36892bb19a42cbe63879d264fa3160275b93b82f7990a152416e1&line=0

你可以看到的是一个不带水印的链接



![alt 你可以看到的是一个不带水印的链接](./img/5.jpg "你可以看到的是一个不带水印的链接")


你可以执行下载等操作 尽情使用吧~
